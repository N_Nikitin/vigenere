package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VigenereEncryptor {

    private static int alphabet = 26;
    private static char alphabetFirstChar = 'A';
    private static String helpMessage = "Input should contain only latin letters without punctuation marks and spaces.";
    private static char[][] matrix = new char[alphabet][alphabet];
    private static boolean isFilled = false;

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
            System.out.println("Enter your MESSAGE or type -help for help:");
            String message = reader.readLine().toUpperCase();
            while (!isInputCorrect(message)){
                if (message.equalsIgnoreCase("-help")) {
                    System.out.println(helpMessage + "\n" + "Enter your MESSAGE or type -help for help:");
                    message = reader.readLine().toUpperCase();
                } else {
                    System.out.println("Incorrect MESSAGE input. Enter correct data or type -help for help:");
                    message = reader.readLine().toUpperCase();
                }
            }
            System.out.println("Enter your KEY or type -help for help:");
            String key = reader.readLine().toUpperCase();
            while (!isInputCorrect(key)){
                if (key.equalsIgnoreCase("-help")) {
                    System.out.println(helpMessage + "\n" + "Enter your KEY or type -help for help:");
                    key = reader.readLine().toUpperCase();
                } else {
                    System.out.println("Incorrect KEY input. Enter correct data or type -help for help:");
                    key = reader.readLine().toUpperCase();
                }
            }
            String encryptionResult = encrypt(message,key);
            String decryptionResult = decrypt(encryptionResult, key);

            System.out.println("Result of encryption: " + encryptionResult);
            System.out.println("Result of decryption: " + decryptionResult);
        } catch (InvalidInputException e) {
            System.out.println(e.getErrorMessage() + "Program will be stopped. Type -help for help.");
        }
        catch (IOException e){
            System.out.println("Caught exception with message: " + e.getMessage());
        }
    }

    private static void fillMatrix(){
        for (int i = 0; i < alphabet; ++i) {
            for (int j = 0; j < alphabet; ++j) {
                matrix[i][j] = (char) ((int) alphabetFirstChar + j + i);
                if ((int) alphabetFirstChar + j + i > (int) alphabetFirstChar + alphabet) matrix[i][j] -= alphabet;
            }
        }
        isFilled = true;
    }

    public static String encrypt(String message, String key) throws InvalidInputException {
        StringBuilder encryptionResult = new StringBuilder();
        if (!isFilled) fillMatrix();
        for (int i = 0; i < message.length(); ++i) {
            if ((int)message.charAt(i) >= (int)'A' && (int)message.charAt(i) <= (int)'Z') {
                encryptionResult.append(matrix[(message.charAt(i) - alphabetFirstChar)][key.charAt(i % key.length()) - alphabetFirstChar]);
            } else throw new InvalidInputException("Invalid MESSAGE input");
        }
        return encryptionResult.toString();
    }

    public static String decrypt(String message, String key) throws InvalidInputException {
        StringBuilder decryptionResult = new StringBuilder();
        if (!isFilled) fillMatrix();
        for (int i = 0; i < message.length(); ++i) {
            if ((int)message.charAt(i) >= (int)'A' && (int)message.charAt(i) <= (int)'Z') {
                for (int j = 0; j < alphabet; ++j) {
                    if (matrix[key.charAt(i % key.length()) - alphabetFirstChar][j] == message.charAt(i)) {
                        decryptionResult.append((char) (j + alphabetFirstChar));
                    }
                }
            } else throw new InvalidInputException("Invalid MESSAGE input");
        }
        return decryptionResult.toString();
    }
    public static boolean isInputCorrect(String string) {
        for (int i = 0; i < string.length(); ++i){
            if (!((int)string.charAt(i) >= (int)'A' && (int)string.charAt(i) <= (int)'Z')) return false;
        }
        return true;
    }
}
