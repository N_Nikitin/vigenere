package com.epam;

public class InvalidInputException extends Exception{

    private String errorMessage;

    public InvalidInputException(String message){
        this.errorMessage = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        this.errorMessage = message;
    }
}
